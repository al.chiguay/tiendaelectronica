from django.contrib import admin
from django.urls import path
from .views import HomeView, OfertaView,VerProductosView, CrearVentaView, MisVentasView

app_name = 'tienda'

urlpatterns = [
    path('', HomeView.as_view(), name="index"),
    path('oferta/', OfertaView.as_view(), name="oferta"),
    path('home-productos/productos/', VerProductosView.as_view(), name="ver-productos"),
    path('venta/', CrearVentaView.as_view(), name="crear-venta"),
    path('mis-ventas/', MisVentasView.as_view(), name="ver-mis-ventas"),

]