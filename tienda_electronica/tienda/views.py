# Create your views here.
from django.shortcuts import render, redirect
from django.views import View
from administracion.models import ProductoModel, VentaModel, ProductoModel, TiendaModel, VendedorModel
from administracion.forms import VentaForm
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
import datetime
from administracion.valor_dolar import InfoApi
from django.contrib import messages


# Create your views here.

class HomeView(View):
    @method_decorator(login_required(login_url='administracion:login'))
    def get(self,request):

        try:
            vendedor = VendedorModel.objects.get(nombre_usuario = request.user.username)
            tienda = vendedor.tienda_asignada.nombre
        except:
            tienda = "No asignada"
        return render(request,'tienda/index.html',{"tienda":tienda})


class OfertaView(View):
    @method_decorator(login_required(login_url='administracion:login'))
    def get(self,request):
        return render(request,'tienda/oferta.html',{})


class VerProductosView(View):
    @method_decorator(login_required(login_url='administracion:login'))
    def get(self, request):

        vendedor = VendedorModel.objects.get(nombre_usuario = request.user.username)
        tienda = vendedor.tienda_asignada

        lista_productos = ProductoModel.objects.filter(tienda_asignada=tienda)

        return render(request, 'tienda/productos.html',{'productos':lista_productos, 'dolar':InfoApi()})


class CrearVentaView(View):
    @method_decorator(login_required(login_url='administracion:login'))
    def get(self, request):        
        #se obtiene el nombre de la tienda del vendedor logeado
        nombre_tienda = VendedorModel.objects.get(nombre_usuario=request.user.username).tienda_asignada.nombre    
        #se obtiene el Objeto Tienda
        tienda = TiendaModel.objects.get(nombre=nombre_tienda)      
        #se obtienen todos los productos disponibles de la tienda
        
        lista_productos=ProductoModel.objects.filter(tienda_asignada=tienda)        
        ventas = VentaModel.objects.all()
        if len(ventas)==0:
            nro_venta = 1
        else:
            orden= ventas.order_by("-nro_venta")
            mayor = orden[0].nro_venta
            if orden[0].estado:
                nro_venta = mayor+1
            else:
                nro_venta = mayor
        productos_agregados = VentaModel.objects.filter(nro_venta=nro_venta)
        context = {"tienda":nombre_tienda,"productos":lista_productos,'vendedor':request.user.username, 
                    "nro_venta":nro_venta, 'productos_agregados':productos_agregados, 'hoy':datetime.datetime.today()}
                    #'productos_agregados':productos_venta,
        return render(request,'tienda/venta.html',context)
    
    @method_decorator(login_required(login_url='administracion:login'))
    def post(self, request):      
        if request.POST["event"]=="agregar":
            #ATRIBUTOS VENTA = nro_venta, tienda, producto, cantidad, total
            if not request.POST.get("lista_productos")=="NOSELECT":
                req = request.POST
                if req.get("cantidad")!="" and req.get("precio")!="" and req.get("total")!="":
                    producto_nombre = request.POST.get("lista_productos").split("-")[0]
                    producto = ProductoModel.objects.get(nombre=producto_nombre)                
                    venta = VentaModel() 
                    info = self.obtener_info(request)          
                    venta.nro_venta= info[0]
                    venta.tienda = info[1]
                    venta.producto = info[2]
                    venta.cantidad = info[3]
                    venta.total = info[4]
                    venta.vendedor = VendedorModel.objects.get(nombre_usuario=request.user.username)
                    venta.estado = False
                    venta.fecha=datetime.datetime.now()
                    venta.total_dolar = round(venta.total/InfoApi(),2)
                    venta.precio_dolar = round(producto.precio/InfoApi(),2)
                    venta.save()
                    return redirect('tienda:crear-venta')
                else:
                    return redirect('tienda:crear-venta')            
            else:
                return redirect('tienda:crear-venta')
        elif "eliminar" in request.POST["event"]:         
            id=request.POST["event"].split("-")[1]
            venta = VentaModel.objects.get(id=id).delete()
            messages.info(request,"Item eliminado correctamente")

            return redirect('tienda:crear-venta')
        elif request.POST["event"]=="cancelar":
            ventas = VentaModel.objects.all()
            if len(ventas)==0:
                nro_venta = 1
            else:
                orden= ventas.order_by("-nro_venta")
                mayor = orden[0].nro_venta
                if orden[0].estado:
                    nro_venta = mayor+1
                else:
                    nro_venta = mayor
            VentaModel.objects.filter(nro_venta=nro_venta).delete()
            messages.info(request,"Venta cancelada")
            return redirect("tienda:index")
        
        elif request.POST["event"]=="guardar":
            ventas = VentaModel.objects.all()
            if len(ventas)==0:
                nro_venta = 1
            else:
                orden= ventas.order_by("-nro_venta")
                mayor = orden[0].nro_venta
                if orden[0].estado:
                    nro_venta = mayor+1
                else:
                    nro_venta = mayor
            if len(VentaModel.objects.filter(nro_venta=nro_venta))>0:
                VentaModel.objects.filter(nro_venta=nro_venta).update(estado=True,fecha=datetime.datetime.now())
                messages.info(request,"Venta realizada exitosamente")
            else:
                messages.info(request,"Debe agregar items para realizar una venta")
                return redirect("tienda:crear-venta")
            return redirect("tienda:index")
        
    def obtener_info(self, request):
        nombre_tienda = VendedorModel.objects.get(nombre_usuario=request.user.username).tienda_asignada.nombre    
        tienda = TiendaModel.objects.get(nombre=nombre_tienda) 
        ventas = VentaModel.objects.all()
        if len(ventas)==0:
            nro_venta = 1
        else:
            orden= ventas.order_by("-nro_venta")
            mayor = orden[0].nro_venta
            if orden[0].estado:
                nro_venta = mayor+1
            else:
                nro_venta = mayor
        nombre_producto = request.POST.get("lista_productos").split("-")[0]
        producto = ProductoModel.objects.get(nombre=nombre_producto)
        cantidad = int(request.POST.get("cantidad"))
        total = cantidad * producto.precio
        return [nro_venta, tienda, producto, cantidad, total]

class MisVentasView(View):
    @method_decorator(login_required(login_url='administracion:login'))
    def get(self, request):     
        vendedor = VendedorModel.objects.get(nombre_usuario=request.user.username)
        dolar = InfoApi()
        ventas = VentaModel.objects.filter(vendedor=vendedor)
        contador_ventas = 0
        total_ventas = 0
        total_ventas_usd = 0
        no_repite = []
        for venta in ventas:
            if venta.nro_venta not in no_repite:
                contador_ventas+=1
                no_repite.append(venta.nro_venta)
            total_ventas += venta.total
            total_ventas_usd += venta.total_dolar
        context = {'vendedor':vendedor,'dolar':dolar,'cantidad_ventas':contador_ventas,
                    'total_ventas':total_ventas,'total_ventas_usd':total_ventas_usd}
        
        return render(request, 'tienda/mis_ventas.html', context) 