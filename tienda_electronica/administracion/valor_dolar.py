import json
import requests
from datetime import datetime
 
def InfoApi():
    # En este caso hacemos la solicitud para el caso de consulta de un indicador en un año determinado
    url = f'https://mindicador.cl/api/'
    response = requests.get(url)
    data = json.loads(response.text.encode("utf-8"))
    return data["dolar"]['valor']
