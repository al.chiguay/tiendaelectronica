from django.shortcuts import render, redirect
from django.views import View
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from .models import TiendaModel, ProductoModel, VendedorModel, OfertaModel, VentaModel
from .forms import RegistroForm, ProductoForm, OfertaForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from .decorators import unauthenticated_user, usuarios_permitidos
from .valor_dolar import InfoApi
# Create your views here.

def logoutUsuario(request):
    logout(request)
    return redirect('administracion:login')


class LoginView(View):
    @method_decorator(unauthenticated_user)
    def get(self, request):
        return render(request, 'administracion/adm_login.html',{})
    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('tienda:index')
        else:
            messages.error(request, 'Usuario o Contraseña incorrecto')
            return render(request, 'administracion/adm_login.html',{})



class RegistroView(View):
    @method_decorator(unauthenticated_user)
    def get(self, request):
        form = RegistroForm()
        lista_tiendas = TiendaModel.objects.all()
        context = {'form':form, 'lista_tiendas':lista_tiendas}
        return render(request, 'administracion/adm_register.html', context)
    def post(self, request):
        form = RegistroForm(data=request.POST)

        if form.is_valid():
            vendedor = VendedorModel()
            vendedor.nombre = request.POST.get("nombre")   
            tienda = TiendaModel.objects.get(nombre=request.POST.get('lista_tiendas'))         
            vendedor.tienda_asignada = tienda
            vendedor.nombre_usuario=request.POST.get('username')
            form.save()
            vendedor.save()
            messages.info(request,"Se ha registrado el usuario correctamente")
            return redirect('administracion:login')
        else:
            lista_tiendas = TiendaModel.objects.all()
            context = {'form':form, 'lista_tiendas':lista_tiendas}
            messages.info(request,"Algun dato no es válido")
            return render(request, 'administracion/adm_register.html', context)
   

class HomeAdminView(View):
    @method_decorator(login_required(login_url='administracion:login'))
    @method_decorator(usuarios_permitidos(roles=['grupo_admin']))     
    def get(self, request):
        return render(request, 'administracion/adm_home.html',{})  
    @method_decorator(login_required(login_url='administracion:login'))
    @method_decorator(usuarios_permitidos(roles=['grupo_admin']))       
    def post(self, request):
        return render(request, 'administracion/adm_home.html',{})

     
class CrearTiendaView(View):       
    @method_decorator(login_required(login_url='administracion:login'))    
    @method_decorator(usuarios_permitidos(roles=['grupo_admin']))
    def get(self, request):        
        return render(request, 'administracion/adm_crear_tienda.html',{})
    @method_decorator(login_required(login_url='administracion:login'))
    @method_decorator(usuarios_permitidos(roles=['grupo_admin']))
    def post(self,request):
        nombre = request.POST.get("nombre")
        direccion = request.POST.get("direccion")
        ciudad = request.POST.get("ciudad")
        comuna = request.POST.get("comuna")
        telefono = request.POST.get("telefono")
        email = request.POST.get("email")
        encargado = request.POST.get("encargado")


        tienda = TiendaModel()
        tienda.nombre = nombre
        tienda.direccion = direccion
        tienda.ciudad = ciudad
        tienda.comuna = comuna
        tienda.telefono = telefono
        tienda.email = email
        tienda.encargado = encargado

        tienda.save() 
        messages.info(request,"Tienda creada existosamente")
        return redirect('administracion:home')

   
class VerTiendasView(View):
    @method_decorator(login_required(login_url='administracion:login'))
    @method_decorator(usuarios_permitidos(roles=['grupo_admin']))
    def get(self, request):
        lista_tiendas = TiendaModel.objects.all()
        return render(request, 'administracion/adm_ver_tiendas.html',{'tiendas':lista_tiendas})



class CrearProductoView(View):
    @method_decorator(login_required(login_url='administracion:login'))
    @method_decorator(usuarios_permitidos(roles=['grupo_admin']))
    def get(self, request):
        producto_form = ProductoForm()
        context = {'producto_form': producto_form}
        return render(request,'administracion/adm_crear_producto.html',context)

    @method_decorator(login_required(login_url='administracion:login'))
    @method_decorator(usuarios_permitidos(roles=['grupo_admin']))
    def post(self, request):
        producto_form = ProductoForm(data=request.POST)
        if producto_form.is_valid():
            producto = producto_form.save(commit=False)
            producto.precio_dolar=round(producto.precio/InfoApi(),2)
            producto.save()
            messages.info(request,"Producto creado exitosamente")
            return redirect("administracion:home")
        else:
            context = {'producto_form': producto_form}
            messages.info(request,"Intente de nuevo")
            return render(request,'administracion/adm_crear_producto.html',context)

class CrearOfertaView(View):
    @method_decorator(login_required(login_url='administracion:login'))
    @method_decorator(usuarios_permitidos(roles=['grupo_admin']))
    def get(self, request):           
        oferta_form = OfertaForm()
        context = {"oferta_form":oferta_form}               
        return render(request,'administracion/adm_crear_oferta.html', context) 
    @method_decorator(login_required(login_url='administracion:login'))
    @method_decorator(usuarios_permitidos(roles=['grupo_admin']))
    def post(self, request):
        oferta_form = OfertaForm(data=request.POST)
        if oferta_form.is_valid():
            oferta_form.save()
            messages.info(request,"Oferta asignada correctamente")
            return redirect('administracion:home')
        else:
            messages.info(request,"Ingrese datos validos")
            context = {"oferta_form":oferta_form}               
            return render(request,'administracion/adm_crear_oferta.html', context) 

class TiendaDescuentoView(View):
    @method_decorator(login_required(login_url='administracion:login'))
    @method_decorator(usuarios_permitidos(roles=['grupo_admin']))
    def get(self, request):  
        tiendas = TiendaModel.objects.all()
        return render(request,'administracion/adm_asignar_descuento_1.html', {"tiendas":tiendas})
    @method_decorator(login_required(login_url='administracion:login'))
    @method_decorator(usuarios_permitidos(roles=['grupo_admin']))
    def post(self, request):   
        if request.POST['page'] == "continuar": 
            tienda = request.POST.get('tiendas')
            tienda_asignada = TiendaModel.objects.get(nombre=tienda)
            productos_tienda = ProductoModel.objects.filter(tienda_asignada=tienda_asignada)
            descto_tienda = OfertaModel.objects.filter(tienda_descuento=tienda_asignada)
            context = {"productos":productos_tienda,'descuentos':descto_tienda}
            return render(request,'administracion/adm_asignar_descuento_1.html', context)
        elif request.POST['page']=="asignar":
            descuento = request.POST.get('descuento')
            producto = request.POST.get('producto')
            tienda_descuento = TiendaModel.objects.get(nombre=descuento.split(", ")[0])
            oferta = OfertaModel.objects.get(tienda_descuento=tienda_descuento, porcentaje_descuento=int(descuento.split(", ")[1].replace("%","")))
            precio = ProductoModel.objects.get(nombre=producto).precio
            ProductoModel.objects.filter(nombre=producto).update(tiene_oferta=oferta, precio=precio*(100-oferta.porcentaje_descuento)/100)          
            messages.info(request,"Descuento asignado en tienda de forma exitosa")
            return redirect("administracion:home")

class VerVentasVendedores(View):
    @method_decorator(login_required(login_url='administracion:login'))
    @method_decorator(usuarios_permitidos(roles=['grupo_admin']))
    def get(self, request):  
        vendedores = VendedorModel.objects.all()
        return render(request,'administracion/adm_ver_ventas.html', {"vendedores":vendedores})
    @method_decorator(login_required(login_url='administracion:login'))
    @method_decorator(usuarios_permitidos(roles=['grupo_admin']))
    def post(self, request):    
        if "ver" in request.POST["event"]:
            id=request.POST["event"].split("-")[1]
            vendedor = VendedorModel.objects.get(id=id)
            ventas_vendedor = VentaModel.objects.filter(vendedor=vendedor).order_by("-fecha")
            total_ventas=0

            
            for venta in ventas_vendedor:
                total_ventas+=venta.total


            total_ventas_usd = round(total_ventas/InfoApi(),2)

            contexto = {'vendedor':vendedor,'ventas':ventas_vendedor,
                        'total_ventas':total_ventas,'total_ventas_usd': total_ventas_usd,
                        'valor_dolar':InfoApi()}
            return render(request,'administracion/adm_venta_vendedor.html', contexto)
        else:
            return redirect('administracion:ver-ventas')


class AsignarVendedorView(View):
    @method_decorator(login_required(login_url='administracion:login'))
    @method_decorator(usuarios_permitidos(roles=['grupo_admin']))
    def get(self, request):
        vendedores = VendedorModel.objects.all()
        return render(request,'administracion/adm_asignar_tienda.html',{'vendedores': vendedores})

    @method_decorator(login_required(login_url='administracion:login'))
    @method_decorator(usuarios_permitidos(roles=['grupo_admin']))
    def post(self,request):
        if "ver" in request.POST["event"]:
            id = request.POST["event"].split("-")[1]
            vendedor = VendedorModel.objects.get(id = id)
            tiendas = TiendaModel.objects.all()

            return render(request,'administracion/adm_asignar_tienda.html',{'vendedor': vendedor, 'tiendas':tiendas})
        
        elif request.POST["event"]=="reasignar":
            id=request.POST.get("vendedor").split("-")[0]


            tienda=request.POST.get("lista_tiendas")
            tienda_bd = TiendaModel.objects.get(nombre=tienda)

            VendedorModel.objects.filter(id=id).update(tienda_asignada=tienda_bd)
            messages.info(request,"Vendedor reasignado")
            return redirect("administracion:home")