from django.contrib import admin
from django.urls import path

from .views import HomeAdminView, CrearTiendaView,  VerTiendasView, RegistroView, LoginView, logoutUsuario, CrearOfertaView,TiendaDescuentoView
from .views import CrearProductoView, VerVentasVendedores, AsignarVendedorView



app_name = 'administracion'

urlpatterns = [
    path('home-adm/', HomeAdminView.as_view(), name="home"),
    path('home-adm/crear-tienda/', CrearTiendaView.as_view(), name="crear-tienda"),
    path('home-adm/crear-oferta/', CrearOfertaView.as_view(), name="crear-oferta"),
    path('home-adm/ver-tiendas/', VerTiendasView.as_view(), name="ver-tiendas"),
    path('registro/', RegistroView.as_view(), name='registro'),
    path('', LoginView.as_view(), name="login"),
    path('logout/', logoutUsuario, name="logout"),
    path('home-adm/crear-producto/', CrearProductoView.as_view(), name="crear-producto"),
    path('home-adm/asignar-oferta/', TiendaDescuentoView.as_view(), name="asignar-oferta"),
    path('home-adm/ver-ventas/', VerVentasVendedores.as_view(), name="ver-ventas"),
    path('home-adm/asignar-vendedor/', AsignarVendedorView.as_view(), name="asignar-vendedor"),
    
    
]