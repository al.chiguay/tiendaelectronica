from django.db import models

# Create your models here.
from django.core.validators import MinValueValidator, MaxValueValidator

class TiendaModel(models.Model):
    nombre = models.CharField(max_length = 200, unique=True, error_messages={"unique":"Este nombre de tienda ya existe"})
    direccion = models.CharField(max_length = 200)
    ciudad = models.CharField(max_length = 200)
    comuna = models.CharField(max_length = 200)
    telefono = models.CharField(max_length = 200)
    email = models.CharField(max_length = 200)
    encargado = models.CharField(max_length = 200)

    def __str__(self):
        return self.nombre

class VendedorModel(models.Model):
    nombre = models.CharField(max_length = 100)
    tienda_asignada = models.ForeignKey(TiendaModel, on_delete=models.CASCADE)    
    nombre_usuario = models.CharField(max_length=100)
    def __str__(self):
        return f"Nombre: {self.nombre} Usuario: {self.nombre_usuario}"    

class OfertaModel(models.Model):
    tienda_descuento = models.ForeignKey(TiendaModel, on_delete=models.CASCADE)
    porcentaje_descuento = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(100)], 
                                            error_messages={"MinValueValidator":"El valor no puede ser menor a 1", "MaxValueValidator": "El valor no puede ser mayor a 100"})
    def __str__(self):
        return f"{self.tienda_descuento}, {self.porcentaje_descuento}%"

class ProductoModel(models.Model):
    nombre = models.CharField(max_length = 100, unique=True, error_messages={"unique":"Este nombre de producto ya existe"})
    descripcion = models.CharField(max_length = 200)
    precio = models.IntegerField(validators=[MinValueValidator(1)])
    precio_dolar = models.FloatField()
    tipo_producto = models.CharField(max_length = 200)
    tienda_asignada = models.ForeignKey(TiendaModel, on_delete=models.CASCADE)
    tiene_oferta = models.ForeignKey(OfertaModel, on_delete=models.CASCADE, blank=True, null=True)
    def __str__(self):
        return self.nombre

class VentaModel(models.Model):
    nro_venta = models.IntegerField()
    tienda = models.ForeignKey(TiendaModel, on_delete=models.CASCADE)
    producto = models.ForeignKey(ProductoModel,on_delete=models.CASCADE)
    cantidad = models.IntegerField(validators=[MinValueValidator(1)])   
    total = models.IntegerField()
    vendedor = models.ForeignKey(VendedorModel, on_delete=models.CASCADE)
    estado = models.BooleanField(default=False)
    fecha = models.DateTimeField()
    precio_dolar = models.FloatField()
    total_dolar = models.FloatField()
    def __str__(self):
        return f"{self.tienda}, ({self.cantidad}) {self.producto}, Total: ${self.total}"




