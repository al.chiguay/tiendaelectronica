from django.contrib import admin
from .models import TiendaModel, VendedorModel, ProductoModel, VentaModel, OfertaModel

# Register your models here.
admin.site.register(TiendaModel)
admin.site.register(VendedorModel)
admin.site.register(ProductoModel)
admin.site.register(VentaModel)
admin.site.register(OfertaModel)
