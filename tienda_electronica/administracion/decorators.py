from django.http import HttpResponse
from django.shortcuts import redirect, render


def unauthenticated_user(view_func):
    def wrapper_func(request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('tienda:index')
        else:
            return view_func(request,*args, **kwargs)
    return wrapper_func

def usuarios_permitidos(roles=[]):
    def decorator(view_func):
        def wrapper_func(request,*args,**kwargs):
            group = None
            if request.user.groups.exists():
                group=request.user.groups.all()[0].name
            if group in roles:                
                return view_func(request,*args,**kwargs)
            else:
                return render(request,'administracion/adm_sinpermisos.html',{})
        return wrapper_func
    return decorator