from django import forms
from django.forms import widgets
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import TiendaModel, ProductoModel, VentaModel, VendedorModel, OfertaModel

class RegistroForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
        
 
    def __init__(self, *args, **kwargs):
        super(RegistroForm, self).__init__(*args,**kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})
        
        self.fields['username'].help_text = None
        self.fields['password1'].help_text = None
        self.fields['password2'].help_text = None

class VentaForm(forms.ModelForm):
    class Meta:
        model = VentaModel
        fields = ['producto', 'cantidad']

    def __init__(self, *args, **kwargs):
        super(VentaForm, self).__init__(*args,**kwargs)       
         
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})
            


class ProductoForm(forms.ModelForm):
    class Meta:
        model = ProductoModel
        fields = ["nombre","descripcion","precio", "tipo_producto", "tienda_asignada"]

    def __init__(self, *args, **kwargs):
        super(ProductoForm, self).__init__(*args,**kwargs) 

        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})

class OfertaForm(forms.ModelForm):
    class Meta:
        model = OfertaModel
        fields = '__all__'
    def __init__(self, *args, **kwargs):
        super(OfertaForm, self).__init__(*args,**kwargs) 

        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})
    

