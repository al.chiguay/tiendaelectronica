Usando la BD guardada ("db.sqlite3") tenemos la siguiente informacion

**Super usuario:**\
Usuario:    admintienda\
Contraseña: admin

**Existen 2 tiendas para hacer pruebas (esto no implica que no se puedan agregar más):**\
-Falaferia\
-Riplay5

Existen 2 vendedores para hacer pruebas (esto no implica que no se puedan registrar nuevos vendedores):\
**VENDEDOR 1**\
Nombre:     Rodrigo Acevedo\
Usuario:    vendedor1\
email:      rodacev@gmail.com\
Contraseña: contrasena1\
Tienda:     Riplay5

**VENDEDOR 2**\
Nombre:     Alfonso Chiguay\
Usuario:    vendedor2\
email:      alfonso.chiguay@gmail.com\
Contraseña: contrasena2\
Tienda:     Falaferia


**Existe un usuario administrador:**\
Nombre:     Sebastián Alvarado\
Usuario:    administrador\
email:      se.alvarado@gmail.com\
Contraseña: contrasena3\
Tienda:     Falaferia

_Se crearon 10 productos que están almacenados en la BD, repartidos 5 por cada tienda.\
Se crearon 2 descuentos, 1 para cada tienda, y estos descuentos se aplicaron a 1 producto por tienda._


**Configuración previa**\
Antes de realizar las pruebas, por favor entrar en http://127.0.0.1:8000/admin/ con las credenciales del superusuario\
mencionadas anteriormente y asegurarse que exista un grupo llamado "grupo_admin". \
Si no existe, por favor crearlo y darle TODOS los permisos. Luego asignar al usuario "administrador" a este grupo.


**IMPORTANTE**\
Una vez realizada esta configuración, no será necesario volver a entrar al panel /admin/, de aquí en adelante \
solo usar usuarios vendedores o administrador.


**Sobre Descuentos/Ofertas**\
Primero se crea un descuento (en porcentaje) con el boton "Crear descuento en tienda" y se asigna a una tienda. Luego de crear \
el descuento, se puede ir a la seccion "Asignar oferta" para desplegar los descuentos previamente creados y asignarlo \
a un producto perteneciente a la tienda elegida.

